# firwalld
[Reference One](https://firewalld.org/documentation/man-pages/firewalld.zone.html)<br />
[Refernece Two](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/sec-Configuring_firewalld.html)<br />
[Reference Three](https://firewalld.org/documentation/)

## Allow ping protocol
firewall-cmd --permanent --zone=public --add-icmp-block=echo-reply

## Adding port with firewalld
firewall-cmd --zone=public --permanent --add-port=80/tcp

## Adding service with firewalld
firewall-cmd --zone=public --permanent --add-service=http

## Exmpale
firewall-cmd --permanent --new-zone=Your_New_Zone
firewall-cmd --reload
firewall-cmd --permanent --set-default-zone=Your_New_Zone
firewall-cmd --permanent --zone=Your_New_Zone --add-icmp-block=echo-reply
firewall-cmd --zone=public --permanent --add-service=https